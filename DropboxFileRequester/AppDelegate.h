//
//  AppDelegate.h
//  DropboxLoginTest
//
//  Created by Tom Davidson on 2/8/16.
//  Copyright © 2016 theMajorDomo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

