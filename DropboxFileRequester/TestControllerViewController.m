//
//  TestControllerViewController.m
//  DropboxLoginTest
//
//  Created by t.j. davidson on 2/11/16.
//  Copyright © 2016 theMajorDomo. All rights reserved.
//

#import "TestControllerViewController.h"
#import "DropboxFileRequester.h"

@interface TestControllerViewController ()

@end

@implementation TestControllerViewController
{
	DropboxFileRequester *_dfr;
	
	// these next two variables are harvested from a login and file request- they can be used
	// for subsequent calls using this manager.
	NSString *_userToken;
	NSUInteger _userID;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
	
	NSLog(@"TestControllerViewController is at %p", self);
}


- (void)viewDidAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	if (nil == _dfr)
	{
		_dfr = [[DropboxFileRequester alloc] initWithPresenter:self destPath:@"/my-upload-folder" withTitle:@"Send me something cool!"];
		//_dfr.userToken = @"a-user-token-from-a-previous-authentication";
		//_dfr.userID = 1234567; // the Dropbox UID
		[_dfr beginOperation:^(DropboxFileRequester *requester, NSDictionary<NSString *,id> *fileCollectorInfo)
		{
			NSLog(@"request completed! file_collector: %@", fileCollectorInfo);
			// save some information from this response that could be used in subsequent
			// file request calls without the need for [visible] authentication
			_userID = ((NSNumber*)fileCollectorInfo[@"user_id"]).unsignedIntegerValue;
			_userToken = fileCollectorInfo[@"token"];
		} cancelHandler:^(DropboxFileRequester *requester)
		{
			NSLog(@"request was cancelled...");
		}];
	}
}

@end
