
console.log('Loading started: epilog.js');


/**
 *  Initialize the code needed to handle a Dropbox RequestFile request.
 */
TMD.initializeRequestFiles = function()
{
    // warm up the file collector code- the IOS code calls methods in this object.
    TMD.fileCollector = new TMDFileCollector();
    
    // we need to intercept XHR requests & responses
    TMD.xhrShim = new XHRShim();
    TMD.xhrShim.addXMLRequestCallback(TMD.fileCollector.requestFired);
    TMD.xhrShim.addXMLResponseCallback(TMD.fileCollector.responseReceived);
}

TMD.initializeRequestFiles();

console.log('Loading completed: epilog.js');
