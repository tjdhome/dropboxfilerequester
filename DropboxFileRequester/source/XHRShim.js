
console.log('Loading started: XHRShim.js');

function XHRShim()
{
    var COMPLETED_READY_STATE = 4;
    var requestCallbacks = [];
    var responseCallbacks = [];
    
    this.addXMLRequestCallback = function addXMLRequestCallback(callback)
    {
        var oldSend, i;
        if(XMLHttpRequest.callbacks)
        {
            // we've already overridden send() so just add the callback
            XMLHttpRequest.callbacks.push( callback );
        }
        else
        {
            // take over .open
            var oldOpen = XMLHttpRequest.prototype.open;
            XMLHttpRequest.prototype.open = function (method, url)
            {
                this.tmdURL = url;
                console.log('XMLHttpRequest.open- URL: ' + url);
                return oldOpen.apply(this, arguments);
            };
            
            XMLHttpRequest.callbacks = [callback];
            
            // take over .send
            oldSend = XMLHttpRequest.prototype.send;
            XMLHttpRequest.prototype.send = function()
            {
                console.log('XMLHttpRequest send was called! URL: ' + this.tmdURL);
                for( i = 0; i < XMLHttpRequest.callbacks.length; i++ )
                {
                    XMLHttpRequest.callbacks[i]( this );
                }
                
                if(this.addEventListener)
                {
                    var self = this;
                    this.addEventListener("readystatechange", function()
                                          {
                                          fireResponseCallbacksIfCompleted(self);
                                          }, false);
                }
                else
                {
                    proxifyOnReadyStateChange(this);
                }
                
                oldSend.apply(this, arguments);
            }
        }
    }
    
    this.addXMLResponseCallback = function addXMLResponseCallback(callback)
    {
        responseCallbacks.push(callback);
    }
    
    function fireCallbacks(callbacks,xhr)
    {
        for( var i = 0; i < callbacks.length; i++ )
        {
            callbacks[i](xhr);
        }
    }
    
    function fireResponseCallbacksIfCompleted(xhr)
    {
        if( xhr.readyState === COMPLETED_READY_STATE )
        {
            console.log('firing response callbacks');
            fireCallbacks(responseCallbacks,xhr);
        }
    }
    
    function proxifyOnReadyStateChange(xhr)
    {
        var realOnReadyStateChange = xhr.onreadystatechange;
        if (realOnReadyStateChange)
        {
            xhr.onreadystatechange = function()
            {
                fireResponseCallbacksIfCompleted(xhr);
                realOnReadyStateChange();
            }
        }
    }
}

console.log('Loading completed: XHRShim.js');
