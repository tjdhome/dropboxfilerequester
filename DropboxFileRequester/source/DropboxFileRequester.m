//
//  DropboxFileRequester.m
//  DropboxLoginTest
//
//  Created by t.j. davidson on 2/11/16.
//  Copyright © 2016 theMajorDomo. All rights reserved.
//

@import WebKit;
#import "DropboxFileRequester.h"
define blab NSLog


static NSString *const xhrHandlerName = @"xhrHandler";
static NSString *const consoleName = @"console";
static NSString *const DropboxLoginEndpoint = @"http://www.dropbox.com/m/login";
static NSString *const DropboxLogoutEndpoint = @"http://www.dropbox.com/m/logout";


@interface NSString(TMDStuff)
- (nullable NSString *)stringByAddingPercentEncodingForFormData:(BOOL)plusForSpace;
@end

@implementation NSString(TMDStuff)

- (nullable NSString *)stringByAddingPercentEncodingForFormData:(BOOL)plusForSpace {
    NSString *unreserved = @"*-._&=";
    NSMutableCharacterSet *allowed = [NSMutableCharacterSet
                                      alphanumericCharacterSet];
    [allowed addCharactersInString:unreserved];
    if (plusForSpace) {
        [allowed addCharactersInString:@" "];
    }
    
    NSString *encoded = [self stringByAddingPercentEncodingWithAllowedCharacters:allowed];
    if (plusForSpace) {
        encoded = [encoded stringByReplacingOccurrencesOfString:@" "
                                                     withString:@"+"];
    }
    return encoded;
}

@end


@implementation DropboxFileRequester
{
	DropboxFileRequesterCancelled _cancelHandler;
	DropboxFileRequesterCompleted _completionHandler;
    WKUserContentController *_contentController;
	BOOL _loginHasBeenHandled;
	BOOL _loginInFlight;
	UIViewController *_standaloneController;
	UIViewController *_presenter;
	NSString *_sharePath;
	NSString *_shareTitle;
	WKWebView *_webview;
}


- (instancetype)initWithPresenter:(UIViewController *)presenter destPath:(NSString *)destPath withTitle:(NSString *)title
{
	self = [super init];
	if (nil != self)
	{
		_presenter = presenter;
		_sharePath = destPath;
		_shareTitle = title;
	}
	
	return self;
}


- (void)initializeStandaloneController
{
	_standaloneController = [UIViewController new];
	NSLog(@"_standaloneController is at %p", _standaloneController);
	//_modalController.modalPresentationStyle = UIModalPresentationFullScreen;
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(onCancelPressed:)];
    _standaloneController.navigationItem.rightBarButtonItem = cancelButton;
	
	[self initializeWebViewWithFrame:CGRectZero];
	_standaloneController.view =_webview;
}


- (void)beginOperation:(DropboxFileRequesterCompleted)completionHandler cancelHandler:(DropboxFileRequesterCancelled)cancelHandler
{
	_completionHandler = completionHandler;
	_cancelHandler = cancelHandler;
	NSURL *pageURL;
	
	// if we were given a userToken & userID, then the caller wants to request file uploads
	// with minimal user interaction.
	
	if ((0 == self.userID) || (nil == self.userToken))
	{
		blab(@"Missing login parameters- presenting modal controller.");
		[self initializeStandaloneController];
        [_presenter.navigationController pushViewController:_standaloneController animated:YES];
		//[_presenter presentViewController:_modalController animated:YES completion:nil];
		pageURL = [NSURL URLWithString:DropboxLogoutEndpoint]; // force a login
	}
	else
	{
		blab(@"Hiding webview under current view.");
		// otherwise, hide the new webview under a pile of views in the presenter
		[self initializeWebViewWithFrame:_presenter.view.frame];
		[_presenter.view insertSubview:_webview atIndex:0];
		pageURL = [NSURL URLWithString:DropboxLoginEndpoint];
	}
	
    _webview.allowsBackForwardNavigationGestures = YES;
	[_webview loadRequest:[NSURLRequest requestWithURL:pageURL]];
}


- (void)cleanup
{
	if (nil == _standaloneController)
	{
		[_webview removeFromSuperview];
	}
	else
	{
		//[_presenter dismissViewControllerAnimated:YES completion:nil];
        [_presenter.navigationController popViewControllerAnimated:YES];
	}
}


- (void)handleLoginCompleted
{
    if (!_loginHasBeenHandled)
    {
        _loginHasBeenHandled = YES;
        // pause to let the page load a bit. This can be tuned. We just need to let the javascript
		// run a bit more after we return. This is because a new page load is likely about to happen.
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^()
        {
            [self onLoginComplete];
        });
    }
}


- (void)initializeWebViewWithFrame:(CGRect)frame
{
    _contentController = [WKUserContentController new];
    WKWebViewConfiguration* webViewConfig = [WKWebViewConfiguration new];
    webViewConfig.userContentController = _contentController;
    _webview = [[WKWebView alloc] initWithFrame:frame configuration:webViewConfig];
    _webview.navigationDelegate = self;
	
    // we're going to inject some code to intercept XHR requests so that can monitor the Dropbox
    // login process.
    NSString *prologCode = [NSString stringWithContentsOfURL:[[NSBundle mainBundle]URLForResource:@"prologCode" withExtension:@"js"] encoding:NSUTF8StringEncoding error:NULL];
    WKUserScript *prologScript = [[WKUserScript alloc]initWithSource:prologCode injectionTime:WKUserScriptInjectionTimeAtDocumentStart forMainFrameOnly:NO];
    [_contentController addUserScript:prologScript];
    
    NSString *xhrShimCode = [NSString stringWithContentsOfURL:[[NSBundle mainBundle]URLForResource:@"XHRShim" withExtension:@"js"] encoding:NSUTF8StringEncoding error:NULL];
    WKUserScript *xhrShimScript = [[WKUserScript alloc]initWithSource:xhrShimCode injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:NO];
    [_contentController addUserScript:xhrShimScript];
    
    NSString *fileCollectorCode = [NSString stringWithContentsOfURL:[[NSBundle mainBundle]URLForResource:@"TMDFileCollector" withExtension:@"js"] encoding:NSUTF8StringEncoding error:NULL];
    WKUserScript *fileCollectorScript = [[WKUserScript alloc]initWithSource:fileCollectorCode injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:NO];
    [_contentController addUserScript:fileCollectorScript];
    
    NSString *epilogCode = [NSString stringWithContentsOfURL:[[NSBundle mainBundle]URLForResource:@"epilogCode" withExtension:@"js"] encoding:NSUTF8StringEncoding error:NULL];
    WKUserScript *epilogScript = [[WKUserScript alloc]initWithSource:epilogCode injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:NO];
    [_contentController addUserScript:epilogScript];
    
    [_contentController addScriptMessageHandler:self name:consoleName];
    [_contentController addScriptMessageHandler:self name:xhrHandlerName];
}


- (void)onCancelPressed:(id)sender
{
	[self cleanup];
	if (nil != _cancelHandler)
		_cancelHandler(self);
}


- (void)onLoginComplete
{
    blab(@"Dropbox login is complete!");
    
    NSString *d1 = [NSString stringWithFormat:@"is_xhr=true&title=%@&path=%@&soft_deadline_ts=-1&hard_deadline_ts=-1&_subject_uid=%lu&t=%@",
                    _shareTitle, _sharePath, (unsigned long)self.userID, self.userToken];
    NSString *formData = [d1 stringByAddingPercentEncodingForFormData:YES];
    blab(@"formData: %@", formData);
    
    // now that everthing is setup, make the request!
    NSString *s = [NSString stringWithFormat:@"TMD.fileCollector.makeDropCreateRequest('%@', '%@');", self.userToken, formData];
    [_webview evaluateJavaScript:s completionHandler:^(id _Nullable result, NSError * _Nullable error) {
        blab(@"r: %@. error: %@", result, error);
    }];
}


/**
 *  Process a callback from our injected js code.
 *
 *  @param userContentController The content controller object.
 *  @param message               The message that was sent to us.
 */
- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message
{
    // if it's a console message, handle that now
    if ([message.name isEqualToString:consoleName])
    {
        blab(@"[js] %@", message.body);
        return;
    }
	
    
    // if it's a XHR-related message, process that now
    if ([message.name isEqualToString:xhrHandlerName])
    {
        NSDictionary<NSString*, NSString*> *pairs = message.body;
        blab(@"XHR message received. %@", pairs);
        NSString *messageType = pairs[@"what"];
        if ([messageType isEqualToString:@"request"] && [messageType isEqualToString:@"/ajax_login"])
        {
            // ah, the js has just initiated the login request!
            _loginInFlight = YES;
            return;
        }
        
        if ([messageType isEqualToString:@"xhr.status"] && !_loginHasBeenHandled)
        {
            // the login request has completed. Let's see what happened.
            // if it was successful, we'll get a subsequent navigation
            // request.
            blab(@"status of login was: %@", pairs[@"thing"]);
        }
        
        if ([messageType isEqualToString:@"xhr.responseJSON"])
        {
            id res = pairs[@"thing"];
            blab(@"JSON: %@", res);
            
            if (!_loginHasBeenHandled)
            {
                [self parseLoginResponse:res];
                return;
            }
            
            if ([res isKindOfClass:[NSDictionary class]])
            {
                NSDictionary *dict = (NSDictionary*)res;
                self.fileCollectorInfo = dict[@"file_collector"];
                if (nil != self.fileCollectorInfo)
                {
                    blab(@"file_collector: %@", self.fileCollectorInfo);
                    if (nil != _completionHandler)
                        _completionHandler(self, self.fileCollectorInfo);
                    [self cleanup];
                }
            }
        }
        
        if ([messageType isEqualToString:@"token"])
        {
            // the login response has been parsed for the login token. We will need this.
			// only save it if it actually has something
			NSString *tok = pairs[@"thing"];
			if (![tok isKindOfClass:[NSNull class]] && (0 != [tok length]))
			{
            	_userToken = pairs[@"thing"];
            	blab(@"accessToken was reported as %@", _userToken);
			}
        }
    }

}

- (void)parseLoginResponse:(id)response
{
    if ([response isKindOfClass:[NSArray class]])
    {
        NSArray *array = (NSArray*)response;
        for (id z in array)
        {
            if ([z isKindOfClass:[NSDictionary class]])
            {
                NSDictionary *d2 = (NSDictionary*)z;
                NSLog(@"new response style: %@", d2);
            }
        }
    }
    
    if ([response isKindOfClass:[NSDictionary class]])
    {
        NSDictionary *dict = (NSDictionary*)response;
        NSString *displayName = dict[@"display_name"];
        if ((nil != displayName) && (![displayName isKindOfClass:[NSNull class]]))
        {
            // the login request has completed. Grab some handy info
            self.loginInfo = dict;
            NSNumber *userID = (NSNumber*)dict[@"id"];
            _userID = userID.unsignedIntegerValue; // we'll need this for the drops/create request
        }
    }
}


/**
 *  Process an attempt to navigate off the login page.
 *
 *  @param webView          The webview running the Dropbox login.
 *  @param navigationAction The requested navigation.
 *  @param decisionHandler  The handler to call when we've decided the fate of the navigation.
 */
- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler
{
    blab(@"navigation requested to: %@", navigationAction.request.URL);
    blab(@"headers: %@", navigationAction.request.allHTTPHeaderFields);
    NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:navigationAction.request.URL];
    blab(@"cookies: %@", cookies);
    
    // If we've have previously authenticated to Dropbox using a WKWebView, then
    // the cookies have already been stored. The js will likely skip the
    // authentication. No matter, WKWebView will have all the cookies we need...
    
    // if we get a navigation to '/m', this means that login has completed
    // and we're being moved to a new page (likely a file listing, but no matter).
    if ([navigationAction.request.URL.path isEqualToString:@"/m"]
        || [navigationAction.request.URL.path isEqualToString:@"/"])
    {
        [self handleLoginCompleted];
        decisionHandler(WKNavigationActionPolicyAllow);
        return;
    }
    
    if (_loginInFlight)
        [self handleLoginCompleted];
    
    decisionHandler(WKNavigationActionPolicyAllow);
}


//  This method does not currently do much. It's a placeholder in case we need to inspect responses
//  from this level. The injected js does most of the work.
- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler
{   
    NSHTTPURLResponse * response = (NSHTTPURLResponse*)navigationResponse.response;
    blab(@"forMainFrame: %d", navigationResponse.forMainFrame);
    blab(@"headers: %@", response.allHeaderFields);

    NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:response.URL];
    blab(@"cookies: %@", cookies);
    
    decisionHandler(WKNavigationResponsePolicyAllow);
}


@end
