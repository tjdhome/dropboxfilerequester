//
//  DropboxFileRequester.h
//  DropboxLoginTest
//
//  Created by t.j. davidson on 2/11/16.
//  Copyright © 2016 theMajorDomo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@import WebKit;
@class DropboxFileRequester;


typedef void (^DropboxFileRequesterCancelled)(DropboxFileRequester *requester);
typedef void (^DropboxFileRequesterCompleted)(DropboxFileRequester *requester, NSDictionary<NSString*,id> *fileCollectorInfo);


/**
 *  A manager for setting up Dropbox-based file requests.
 */
@interface DropboxFileRequester : NSObject<WKNavigationDelegate,WKScriptMessageHandler,WKUIDelegate>


/**
 *  If the file request was completed, this will contain a portion of
 *  the link required and some other information peraining to the operation.
 */
@property NSDictionary<NSString*,id> *fileCollectorInfo;


/**
 *  If a login was performed, this will contain some interesting information.
 */
@property NSDictionary<NSString*,id> *loginInfo;


/**
 *  The user's login token (perhaps from a previous authentication)
 */
@property NSString *userToken;


/**
 *  The user id (perhaps from a previous authentication)
 */
@property NSUInteger userID;


/**
 *  Initialize the requester.
 *
 *  @param presenter THe view controller that represents our caller.
 *  @param destPath  The destination path in Dropbox to receive the files.
 *  @param title     A text string which will be used by Dropbox to describe the reqest
 *					to the user. Ordinarily only the owner will see any evidence of this and
 *					only id the owner user logs into Dropbox and views their file requests.
 *
 *  @return An initialized object.
 */
- (instancetype)initWithPresenter:(UIViewController*)presenter destPath:(NSString*)destPath withTitle:(NSString*)title;


/**
 *  Begin the file request operation. If the user credentials are either in error or not presented
 *	then this manager may present a modal to invite a Dropbox login.
 *
 *  @param completionHandler A block to call when the file request completes.
 *  @param cancelHandler     A block to call if the file request fails.
 */
- (void)beginOperation:(DropboxFileRequesterCompleted)completionHandler cancelHandler:(DropboxFileRequesterCancelled)cancelHandler;

@end
