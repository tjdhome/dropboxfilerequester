
console.log('Loading started: TMDFileCollector.js');

function TMDFileCollector()
{
    function getCookie(name)
    {
        var re = new RegExp(name + "=([^;]+)");
        var value = re.exec(document.cookie);
        return (value != null) ? unescape(value[1]) : null;
    }

    
    this.makeDropCreateRequest = function(token, formData)
    {
        console.log('about to construct an XMLHttpRequest. token: ' + token);
        var xhr = new XMLHttpRequest();
        
        xhr.open('POST', 'https://www.dropbox.com/drops/create');
        
        xhr.setRequestHeader('x-requested-with', 'XMLHttpRequest');
        xhr.setRequestHeader('content-type', 'application/x-www-form-urlencoded; charset=UTF-8');
        xhr.setRequestHeader('accept', 'application/json, text/javascript, */*; q=0.01');
        
        
        console.log('about to send XMLHttpRequest.');
        console.log('formData: ' + formData);
        xhr.send(formData);
    }

    
    this.requestFired = function(xhr)
    {
        try {
            webkit.messageHandlers.xhrHandler.postMessage({ what: 'request', thing: xhr._url});
        } catch(e)
        {
            console.log('ACK- ' + e);
        }
    }

    this.responseReceived = function(xhr)
    {
        console.log('hey! got a response. ' + typeof(xhr));
        console.log('  type: ' + xhr.responseType);
        console.log('  headers: ' + xhr.getAllResponseHeaders());
        console.log('  cookie: ' + document.cookie);
        console.log('  document.domain: ' + document.domain);
        console.log('  responseText: ' + xhr.responseText);
        console.log('  typeof response- ' + typeof(xhr.response));
        
        var token = getCookie('__Host-js_csrf');
        console.log('__Host-js_csrf: ' + token);
        
        try {
            webkit.messageHandlers.xhrHandler.postMessage({ what: 'token', thing: token});
            webkit.messageHandlers.xhrHandler.postMessage({ what: 'xhr.response', thing: xhr.response});
            webkit.messageHandlers.xhrHandler.postMessage({ what: 'xhr.status', thing: xhr.status});
            
            // this is for JSON-based responses. OK if it fails..
            var json = JSON.parse(xhr.responseText);
            webkit.messageHandlers.xhrHandler.postMessage({ what: 'xhr.responseJSON', thing: json});
        } catch(e)
        {
            console.log('ACK- ' + e);
        }
    }
}


console.log('Loading completed: TMDFileCollector.js');
