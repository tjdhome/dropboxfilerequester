

var TMD = new Object();


/**
 *  Write some output back to the IOS app.
 *
 *  @param what The stuff to write.
 */
TMD.writeToConsole = function(what)
{
    try {
        webkit.messageHandlers.console.postMessage(what);
    }
    catch(e)
    {
        //
    }
};


/**
 *  Do nothing - a standin for console.s sink.
 *
 *  @param what The stuff that might have been written.
 */
TMD.noop = function(what)
{ }


// take over the console, so we can see what is happening
console.log = TMD.writeToConsole;
//console.log = TMD.noop;

console.log('Loading completed: prolog.js');
