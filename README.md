# DropboxFileRequester
A sample chunk-o-code to support api access to Dropbox File Requests.

Dropbox does not currently support API access to file requests- that ability for non-Dropbox users to contribute files to a Dropbox folder. This feature has been requested of Dropbox, but has not yet appeared in a roadmap or release. Until that time, this hunk of code demonstrates the ability to originate a Dropbox file request from an IOS application.

It uses a webview to issue a file request operation (possibly prefaced by an visible (and standard Dropbox) authentication dialog).

Comments and suggestions are most welcome.